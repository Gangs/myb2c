<?php
include ROOT.'./smarty/Smarty.class.php';
$tpl=new Smarty();
$tpl->template_dir=ROOT."./templates/";
$tpl->compile_dir=ROOT."./compiles/";
$tpl->setCaching(false);
$tpl->setCacheDir(ROOT.".data/smartycaches/");    
$tpl->setCacheLifetime(3600);
$tpl->left_delimiter="<{";
$tpl->right_delimiter="}>";
$tpl->assign("config",$C);
$tpl->assign("G",$G);
