<?php
class Adminhelp {
    private $effectime="3600";
    //检测登陆状态
    function mcheckol(){
        global $help,$db;
        $nowtime=time();
        $msid=$help->authcode($_COOKIE['msid'],'DECODE');
        if($msid){
            $q=$db->fetch_first("select * from adminsession where msid='{$msid}' limit 0,1");
            if($q){
                if($q['lastolup']<($nowtime-$this->effectime)){
                    $db->query("delete from adminsession where msid='{$msid}'");
                    
                    return FALSE;
                }
                if($q['olip']!=$_SERVER['REMOTE_ADDR']){
                    
                    return FALSE;
                }
                return $q;
            }else{
                
                return FALSE;
            }
        }else{
            
            return FALSE;
        }
    }
    //创建session
    function mcreatesession($adminid){
        global $C;
        if(!$this->mcheckol()){
            if(!isset($adminid)){
                return FALSE;
            }          
            global $db,$help;
            $nowtime=time();
            $olip=isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:0;
            $str1=$help->mkrandomstr();
            $str=$help->authcode($str1,"ENCODE");
            unset($_COOKIE["msid"]);
            unset($_COOKIE["meffec"]);
            setcookie("msid",$str,$nowtime+$this->effectime,"/");                        
            setcookie("meffec",$nowtime+$this->effectime,$nowtime+$this->effectime,"/");
            $db->query("delete from adminsession where adminid='{$adminid}'");
            $db->query("replace into adminsession (msid,adminid,lastolup,olip) values ('{$str1}','{$adminid}','{$nowtime}','{$olip}')");
            return TRUE;
        }else{
            return TRUE;
        }
    }
    //更新SESSION
    function mupdatesession(){
        global $help,$db;
        $nowtime=time();
        if(($q=$this->mcheckol()) && is_array($q)){           
            @setcookie("msid",$help->authcode($q['msid'],'ENCODE'),$nowtime+$this->effectime,"/");
            @setcookie("meffec",$nowtime+$this->effectime,$nowtime+$this->effectime,"/");
            $db->query("update adminsession set lastolup='{$nowtime}' where msid='{$q['msid']}'");
            return TRUE;
        }else{
            return FALSE;
        }
    }
}

