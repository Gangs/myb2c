-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 11 月 20 日 06:01
-- 服务器版本: 5.1.41
-- PHP 版本: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `myb2c`
--

-- --------------------------------------------------------

--
-- 表的结构 `adminid`
--

CREATE TABLE IF NOT EXISTS `adminid` (
  `adminid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(15) NOT NULL,
  `userpw` char(32) NOT NULL,
  `title` char(32) NOT NULL,
  `rank` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `mtypes` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `mcommos` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `mmembers` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `morders` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `manns` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `mlinks` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastip` char(15) NOT NULL DEFAULT '0',
  `nowip` char(15) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adminid`),
  KEY `adminid` (`adminid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `adminid`
--

INSERT INTO `adminid` (`adminid`, `username`, `userpw`, `title`, `rank`, `mtypes`, `mcommos`, `mmembers`, `morders`, `manns`, `mlinks`, `lastip`, `nowip`) VALUES
(2, 'admin', 'aa533c0fda25e6335f2bff76ac1941d2', '创始人', 10, 1, 1, 1, 1, 1, 1, '::1', '::1');

-- --------------------------------------------------------

--
-- 表的结构 `adminsession`
--

CREATE TABLE IF NOT EXISTS `adminsession` (
  `msid` char(6) NOT NULL,
  `adminid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lastolup` int(10) unsigned NOT NULL DEFAULT '0',
  `olip` char(15) NOT NULL DEFAULT '0',
  KEY `msid` (`msid`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `adminsession`
--


-- --------------------------------------------------------

--
-- 表的结构 `ann`
--

CREATE TABLE IF NOT EXISTS `ann` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `ann`
--

INSERT INTO `ann` (`id`, `title`, `content`, `time`) VALUES
(1, '111111', '11111111', 0),
(2, '222', '2222', 1347875884),
(4, '2222222', '222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n2222222222222222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n222222222222222222222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n222222222222222222222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n222222222222222222222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n222222222222222222222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n222222222222222222222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n222222222222222222222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n222222222222222222222222\r\n222222\r\n222222\r\n222222\r\n222222222222\r\n222222\r\n222222222222222222\r\n\r\n\r\n22222222222222222222222', 0),
(5, '33333', '333333', 1348307363);

-- --------------------------------------------------------

--
-- 表的结构 `attachment`
--

CREATE TABLE IF NOT EXISTS `attachment` (
  `aid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(100) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `filesize` int(10) unsigned NOT NULL,
  `commoid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `typeid` int(4) unsigned NOT NULL DEFAULT '0',
  `dateline` int(10) unsigned NOT NULL,
  `path` varchar(100) NOT NULL,
  `isimg` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`),
  KEY `commoid` (`commoid`,`typeid`),
  KEY `isimg` (`isimg`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- 转存表中的数据 `attachment`
--

INSERT INTO `attachment` (`aid`, `filename`, `filetype`, `filesize`, `commoid`, `typeid`, `dateline`, `path`, `isimg`) VALUES
(64, 'Koala.jpg', 'application/octet-stream', 780831, 18, 25, 1349505835, 'images/132926c1070197e02431c56164144dc5.jpg', 1),
(63, 'Chrysanthemum.jpg', 'application/octet-stream', 879394, 18, 25, 1349505835, 'images/9bdef6b649a9cfe515f8fa1d36f904a7.jpg', 1),
(62, 'Penguins.jpg', 'application/octet-stream', 777835, 18, 25, 1349505828, 'images/efc28b1bd69a759258f2e3de8ca099d3.jpg', 1),
(61, 'Lighthouse.jpg', 'application/octet-stream', 561276, 18, 25, 1349505828, 'images/2c68e1bec72b1d9a064dbba8428fdd56.jpg', 1),
(58, 'Desert.jpg', 'application/octet-stream', 845941, 16, 20, 1349231251, 'images/3dff6572319a3abb3334746458f493cb.jpg', 1),
(59, 'Penguins.jpg', 'application/octet-stream', 777835, 17, 23, 1349231289, 'images/636e74a782291ac5b68fbb22ca8e4669.jpg', 1),
(60, 'blue_glow.jpg', 'application/octet-stream', 164474, 18, 25, 1349505501, 'images/0dbb8410e9eaca248c935c9ddc326e14.jpg', 1);

-- --------------------------------------------------------

--
-- 表的结构 `commo`
--

CREATE TABLE IF NOT EXISTS `commo` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `seoname` varchar(50) NOT NULL,
  `info` text NOT NULL,
  `typeid` int(4) unsigned NOT NULL DEFAULT '0',
  `price` float unsigned NOT NULL DEFAULT '0',
  `vprice` float unsigned NOT NULL DEFAULT '0',
  `discount` tinyint(1) NOT NULL DEFAULT '0',
  `stock` int(4) unsigned NOT NULL DEFAULT '0',
  `sell` int(8) unsigned NOT NULL DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `typeid` (`typeid`),
  KEY `discount` (`discount`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `commo`
--

INSERT INTO `commo` (`id`, `name`, `seoname`, `info`, `typeid`, `price`, `vprice`, `discount`, `stock`, `sell`, `addtime`) VALUES
(16, '11111111', '1111111', '<p>\r\n	商品1</p>\r\n', 20, 100, 85.9, 0, 10, 1, 1349231260),
(17, '222222', '222222', '<p>\r\n	1111111111111111111111111111111111</p>\r\n', 25, 22, 11, 5, 7, 0, 1349231299),
(18, '3333333333', '333333333', '<p style="text-align: center; ">\r\n	<span style="background-color:#ff0000;">3333333333</span></p>\r\n<p style="text-align: center; ">\r\n	&nbsp;</p>\r\n<p style="text-align: center; ">\r\n	&nbsp;</p>\r\n<p style="text-align: center; ">\r\n	<span style="background-color:#ff0000;">1</span></p>\r\n<p style="text-align: center; ">\r\n	&nbsp;</p>\r\n<p style="text-align: center; ">\r\n	&nbsp;</p>\r\n<p style="text-align: center; ">\r\n	&nbsp;</p>\r\n<p style="text-align: center; ">\r\n	<img alt="" src="http://localhost/b2c/img.php?aid=60" style="width: 1024px; height: 768px; " /></p>\r\n<p style="text-align: center; ">\r\n	&nbsp;</p>\r\n<p style="text-align: center; ">\r\n	<span style="background-color:#ff0000;">111111</span></p>\r\n', 25, 33, 3, 0, 9, 4, 1349231596),
(19, '222222', '2222222', '<p>\r\n	222</p>\r\n', 24, 22, 2, 3, 0, 0, 1349235924),
(20, '123123123', '123123', '<p>\r\n	111</p>\r\n', 25, 111, 1, 0, 6, 0, 1349511108);

-- --------------------------------------------------------

--
-- 表的结构 `favorite`
--

CREATE TABLE IF NOT EXISTS `favorite` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `commoid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`,`commoid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `favorite`
--

INSERT INTO `favorite` (`id`, `uid`, `commoid`) VALUES
(2, 4, 19);

-- --------------------------------------------------------

--
-- 表的结构 `flink`
--

CREATE TABLE IF NOT EXISTS `flink` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `href` varchar(200) NOT NULL,
  `title` varchar(255) NOT NULL,
  `color` varchar(30) NOT NULL,
  `strong` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `logourl` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `flink`
--

INSERT INTO `flink` (`id`, `name`, `href`, `title`, `color`, `strong`, `logourl`) VALUES
(2, '百度', 'http://www.baidu.com', '度娘', '#fa0505', 1, 'http://www.baidu.com/img/918-s_8ff85f603442dbb80422486c85bb5c8e.gif'),
(4, '谷歌', 'http://www.google.cn', '最好的搜索引擎', '', 1, ''),
(5, '网易', 'http://www.163.com', '网易', '', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `lunbo`
--

CREATE TABLE IF NOT EXISTS `lunbo` (
  `id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `src` char(200) NOT NULL,
  `href` char(200) NOT NULL,
  `des` char(200) NOT NULL,
  `time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `lunbo`
--

INSERT INTO `lunbo` (`id`, `src`, `href`, `des`, `time`) VALUES
(7, 'http://images.cnblogs.com/cnblogs_com/babyzone2004/256463/o_p3.jpg', '#', '22222', 1349265839),
(4, 'http://images.cnblogs.com/cnblogs_com/babyzone2004/256463/o_p5.jpg', '#', '无', 1349265811),
(6, 'http://images.cnblogs.com/cnblogs_com/babyzone2004/256463/o_p4.jpg', '#', '111111', 1349265822),
(8, 'http://images.cnblogs.com/cnblogs_com/babyzone2004/256463/o_p1.jpg', '#', '无', 1349267079);

-- --------------------------------------------------------

--
-- 表的结构 `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `uid` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `pw` char(32) NOT NULL,
  `lastip` varchar(100) NOT NULL,
  `olip` varchar(100) NOT NULL,
  `regtime` int(10) unsigned NOT NULL,
  `money` float NOT NULL DEFAULT '0',
  `extcredits1` int(8) NOT NULL DEFAULT '0',
  `ask1` varchar(100) NOT NULL,
  `ans1` varchar(100) NOT NULL,
  `ask2` varchar(100) NOT NULL,
  `ans2` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `email` char(100) NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `member`
--

INSERT INTO `member` (`uid`, `name`, `pw`, `lastip`, `olip`, `regtime`, `money`, `extcredits1`, `ask1`, `ans1`, `ask2`, `ans2`, `status`, `email`) VALUES
(4, 'zstxt1989', 'bf7fc070b72a549a3f882cb3c0eab8bc', '::1', '::1', 1348502269, 9999, 0, '提示1', '回答1', '提示2', '回答2', 0, '123456111@qq.com'),
(5, 'zstxt1989a', 'aa533c0fda25e6335f2bff76ac1941d2', '', '::1', 1348502448, 0, 0, '111', '111', '111', '111', 0, '1@qq.com'),
(6, 'zstxt1989aa', 'aa533c0fda25e6335f2bff76ac1941d2', '', '::1', 1348502604, 0, 0, '2', '2', '2', '2', 0, 'aaa@qq.com');

-- --------------------------------------------------------

--
-- 表的结构 `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `item` varchar(1000) NOT NULL,
  `price` float unsigned NOT NULL,
  `time` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `address` char(250) NOT NULL,
  `paytime` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- 转存表中的数据 `order`
--

INSERT INTO `order` (`id`, `uid`, `item`, `price`, `time`, `status`, `address`, `paytime`) VALUES
(16, 4, 'a:2:{i:0;a:2:{s:2:"id";s:2:"20";s:5:"count";s:1:"1";}i:1;a:2:{s:2:"id";s:2:"17";s:5:"count";s:1:"2";}}', 12, 1350354827, 0, '', 0),
(14, 4, 'a:1:{i:0;a:2:{s:2:"id";s:2:"20";s:5:"count";s:1:"1";}}', 1, 1350008477, 1, '联系电话：111111111 收货地址：111111111', 1350008511),
(15, 4, 'a:1:{i:0;a:2:{s:2:"id";s:2:"20";s:5:"count";s:1:"1";}}', 1, 1350008505, 0, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL,
  `seotitle` char(100) NOT NULL,
  `des` char(255) NOT NULL,
  `pid` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `path` varchar(200) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent` (`path`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- 转存表中的数据 `types`
--

INSERT INTO `types` (`id`, `name`, `seotitle`, `des`, `pid`, `path`) VALUES
(23, '手机', '', '', 20, '0-20'),
(24, '软件', '', '', 21, '0-21'),
(25, '智能机', '', '', 23, '0-20-23'),
(26, '功能机', '', '', 23, '0-20-23'),
(27, '生活', '', '', 21, '0-21'),
(28, '杂志', '', '', 21, '0-21'),
(20, '电子商品', '', '', 0, '0'),
(21, '图书', '', '', 0, '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
