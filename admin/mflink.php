<?php
include '../include/common.inc.php';
$adminhelp=new Adminhelp();
if(!$q=$adminhelp->mcheckol()){
    header("Location:".$C['SITE_URL']."/admin/login.php");
    exit;
}
$adminhelp->mupdatesession();
if(!$action){
    $perpage=10;
    $pagevar="page";
    $q2=$db->query("select id from flink");
    
    $pages=new Fpage();
    $pages->totalNums=$db->num_rows($q2);
    $pages->perpageNum=$perpage;
    $pages->pageVar=$pagevar;
    $pages->jump_pageinputId="jumppage";
    $pagestr=$pages->showpages();
    $page=($$pagevar && is_numeric($$pagevar))?$$pagevar:0;
    $leftstart=$perpage*(max(0,$page-1));
    $q3=$db->query("select * from flink order by id desc limit {$leftstart},{$perpage} ");
    $flinklist=array();
    while($row=$db->fetch_array($q3)){       
        $flinklist[]=$row;
    }
    unset($q3);
    $tpl->assign("flinklist",$flinklist);
    $tpl->assign("page",$pagestr);
    $tpl->display("admin/mflink.html");
}elseif($action=="add"){
    if(!$help->submitcheck()){
        
        $tpl->display("admin/mflink_add.html");
        exit;
    }
    if(!$name){
        exit("未输入网站名");
    }
    if(!$href){
        exit("未输入网站地址");        
    }
    $logourl=$logourl?$logourl:"";
    $color=$color?$color:"";
    $strong=$strong?$strong:0;
    $title=$title?$title:"";
    
    $db->query("insert into flink (name,href,title,color,strong,logourl) values ('{$name}','{$href}','{$title}','{$color}',{$strong},'{$logourl}')");
    $help->showmessage("添加成功",$C['SITE_URL'].'/admin/mflink.php',3);
}elseif($action=="edit"){
    if(!$flinkid || !is_numeric($flinkid)){
        exit("未指定ID，或ID不合法");
    }
    if(!$submit){
        $q=$db->fetch_first("select * from flink where id={$flinkid}");
        $tpl->assign("flinkinfo",$q);
        unset($q);
        $tpl->display("admin/mflink_edit.html");
    }else{
        if(!$name){
            exit("未输入网站名");
        }
        if(!$href){
            exit("未输入网站地址");        
        }
        $logourl=$logourl?$logourl:"";
        $color=$color?$color:"";
        $strong=$strong?$strong:0;
        $title=$title?$title:"";
        $db->query("update flink set name='{$name}',href='{$href}',title='{$title}',color='{$color}',strong={$strong},logourl='{$logourl}' where id={$flinkid}");
        $help->showmessage("修改成功",$C['SITE_URL'].'/admin/mflink.php',3);
    }
}elseif($action=="delete"){
    if(!$flinkid || !is_numeric($flinkid)){
        exit("未指定ID，或ID不合法");
    }
    $db->query("delete from flink where id={$flinkid}");
    $help->showmessage("删除成功",$C['SITE_URL'].'/admin/mflink.php',3);
}
