<?php
include './include/common.inc.php';
if(!$U['uid']){
    $help->showmessage("请先登陆！",$C['SITE_URL']);
}
if(!$m){
    if($index=='index'){
        
        $U = $db->fetch_first("select m.* from member m where m.uid={$U['uid']}");

        $q = $db->fetch_first("select count(uid) as count from `order` where uid={$U['uid']} and status=0");
        $U['count'] = $q['count'];
        unset($q);
        
        $tpl->assign("U", $U);
        $tpl->display("myhome_index.html");
    }  else {            
        $tpl->display("myhome.html");
    }
}else{
    if($m=='myinfo'){
        if($submit){
            $msg='<font color=green>保存成功</font>';
            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $msg='邮箱格式不合法';
            }elseif((isset($newpw) && (!isset ($newpw1))) || (isset($newpw) && isset ($newpw1) && $newpw!=$newpw1)){
                $msg='两次输入的新密码不一致';
            }elseif(!$ask1){
                $msg='提示问题1不能为空';
            }elseif(!$ans1){
                $msg='回答1不能为空';
            }elseif(!$ask2){
                $msg='提示问题2不能为空';                                              
            }elseif(!$ans2){
                $msg='回答2不能为空';
            }else{
                $ext='';
                if($newpw){
                    $newpw=md5($newpw);
                    $ext=",pw='{$newpw}'";
                }
                $db->query("update member set email='{$email}',ask1='{$ask1}',ans1='{$ans1}',ask2='{$ask2}',ans2='{$ans2}' {$ext} where uid ={$U['uid']}");
            }     
            $tpl->assign("msg",$msg);
        }
        
        $member=$db->fetch_first("select * from member where uid = {$U['uid']}");
        $tpl->assign("member",$member);
        $tpl->display("myhome_myinfo.html");
    }elseif($m=='myorder'){
        $where="";
        if(isset($s) && $s==0){
            $where .= " AND status=0";
        }
        if(isset($s) && $s==1){
            $where .= " AND status=1";
        }
        if(isset($s) && $s==2){
            $where.=" AND status=2";
        }
        
        /*
         * 分页显示
         */
        $perpage = 10;
        $pagevar = "page";
        $nowtime = time();
        $q2 = $db->query("select id from `order` where uid={$U['uid']} {$where}");

        $pages = new Fpage();
        $pages->totalNums = $db->num_rows($q2);
        $pages->perpageNum = $perpage;
        $pages->pageVar = $pagevar;
        $pages->jump_pageinputId = "jumppage";
        $pagestr = $pages->showpages();
        $page = ($$pagevar && is_numeric($$pagevar)) ? $$pagevar : 0;
        $leftstart = $perpage * (max(0, $page - 1));
        $q3 = $db->query("select * from `order` where uid={$U['uid']} {$where} order by id desc limit {$leftstart},{$perpage}");
        $orderlist = array();
        while ($row = $db->fetch_array($q3)) {
            $row['item']=  unserialize(stripslashes($row['item']));
            $item_str="";
            if($row['item']){
                foreach($row['item'] as $v){
                    $cq = $db->fetch_first("select id,name from commo where id = {$v['id']}");
                    $item_str.='<a href=' . $C['SITE_URL'] . '/commo.php?id=' . $cq['id'] . '><b>' . $cq['name'] . '</b></a> <font color=green>x ' . $v['count'] . '件</font><br>';
                    unset($cq);
                }
            }
            $row['item']=$item_str;
            $orderlist[] = $row;
        }
        unset($q3);
        
        $tpl->assign("orderlist", $orderlist);
        $tpl->assign("page", $pagestr);
        $tpl->display("myorder.html");
        
    }elseif($m=='myfavorite'){
        if($delid && is_numeric($delid)){
            $db->query("delete from favorite where uid = {$U['uid']} and commoid={$delid}");
            echo '1';       
            exit;
        }
         /*
         * 分页显示
         */
        $perpage = 10;
        $pagevar = "page";
        $nowtime = time();
        $q2 = $db->query("select id from `favorite` where uid={$U['uid']}");

        $pages = new Fpage();
        $pages->totalNums = $db->num_rows($q2);
        $pages->perpageNum = $perpage;
        $pages->pageVar = $pagevar;
        $pages->jump_pageinputId = "jumppage";
        $pagestr = $pages->showpages();
        $page = ($$pagevar && is_numeric($$pagevar)) ? $$pagevar : 0;
        $leftstart = $perpage * (max(0, $page - 1));
        $q3 = $db->query("select f.*,c.name from `favorite` f,commo c where f.uid={$U['uid']} and c.id=f.commoid order by f.id desc limit {$leftstart},{$perpage}");
        
        
        
      
        $favolist=array();
        
        while($row = $db->fetch_array($q3)){
         $favolist[]=$row;   
        }
        unset($q3);
        $tpl->assign("favolist",$favolist);
        $tpl->assign("page",$pagestr);
        $tpl->display("myfavorite.html");
    }else{
        header('HTTP/1.1 404 Not Found');
        $tpl->display("404.html");
    }
}