<?php
include './include/common.inc.php';

    $err=false;
    if(strlen($keywords)<2 || strlen($keywords)>12){
        $err='关键词太短或太长，2-12个字符';
    }
    
    
    if(!$err){
        $perpage = 12;
        $pagevar = "page";
        $nowtime = time();
        $q2 = $db->query("select id from commo where name like '%{$keywords}%'");

        $pages = new Fpage();
        $pages->totalNums = $db->num_rows($q2);
        $pages->perpageNum = $perpage;
        $pages->pageVar = $pagevar;
        $pages->jump_pageinputId = "jumppage";
        $pagestr = $pages->showpages();
        $page = ($$pagevar && is_numeric($$pagevar)) ? $$pagevar : 0;
        $leftstart = $perpage * (max(0, $page - 1));
        $q3 = $db->query("select  commo.*,attachment.path,attachment.isimg,types.name as typename from commo LEFT JOIN types on commo.typeid=types.id LEFT JOIN attachment on attachment.commoid=commo.id where commo.name like '%{$keywords}%' group by commo.id order by commo.id desc limit {$leftstart},{$perpage}");
        $commolist = array();
        while ($row = $db->fetch_array($q3)) {
            $commolist[] = $row;
        }
        unset($q3);
        $tpl->assign("commolist", $commolist);
        $tpl->assign("page", $pagestr);
    }
    $tpl->assign("keywords",$keywords);
    if(!$keywords){
        $title='搜索商品';
    }else{
        $title='搜索 '.$keywords.' 的结果';
    }
    $tpl->assign("title",$title);
    $tpl->assign("err",$err);
    
    $tpl->display("search_re.html");
    
