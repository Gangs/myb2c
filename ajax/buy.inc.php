<?php
if(!$ajax){
    exit;
}
if($m && $va){
    $m($va);       
    exit;
}
if(!$commoid || !is_numeric($commoid) || !$count || !is_numeric($count)){
    echo '1';
    exit;
}
$q=$db->fetch_first("select stock from commo where id={$commoid}");
if(!$q){
    echo '2';//商品不存在
    exit;
}
if(($q['stock']-$count)<0){
    echo '3';//库存不足
    exit;
}
$arr=array();
$cookie=$_COOKIE['buycommo'];
if($cookie){
    $a=unserialize(stripslashes($cookie));
    if(is_array($a)){
        $arr=$a;
        $arr[$commoid]=$count;
        
        $cookie=serialize($arr);
        setcookie('buycommo', $cookie,time()+36000,"/");
        echo '0';
        exit;
    }else{
        $arr[$commoid]=$count;
        $cookie=serialize($arr);
        setcookie('buycommo', $cookie,time()+36000,"/");
        echo '0';
        exit;
    }
}else{
    $arr[$commoid]=$count;
    $cookie = serialize($arr);
    setcookie('buycommo', $cookie,time()+36000, "/");
    echo '0';
    exit;
}
    

function rm($id){
    $cookie=$_COOKIE['buycommo'];
    $arr =  unserialize(stripslashes($cookie));
    
    unset($arr[$id]);
    setcookie("buycommo",serialize($arr),time()+36000,"/");
    echo 'ok';
}