<?php
if(!$ajax){
    exit;
}
if(!$U['uid']){
    echo '请先登陆';
    exit;
}
if(!$commoid || !is_numeric($commoid)){
    echo '非法ID参数';
    exit;
}
$q=$db->fetch_first("select id from `favorite` where uid = {$U['uid']} and commoid={$commoid}");
if($q){
    echo '您已经收藏过该商品，请勿重复收藏！';
    exit;
}
unset($q);

$db->query("insert into `favorite` (uid,commoid) values ({$U['uid']},{$commoid})");
echo '收藏成功！';

